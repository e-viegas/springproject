/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author formation
 */
@Entity
@Table(name = "people")
public class Person {
    /**
     * Unique ID of each person
     */
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;
    
    /**
     * Family name of the person
     */
    @Column(name = "familyname")
    private final String familyName;
    
    /**
     * First name of the person
     */
    @Column(name = "firstname")
    private final String firstName;
    
    /**
     * Email of the person
     */
    @Column(name = "email")
    private final String email;
    
    /**
     * Birthday of the person
     */
    @Column(name = "birthday")
    private final String birthday;
    
    /**
     * Organisation for which this person works
     */
    @Column(name = "organisation")
    private final String organisation;
    
    /**
     * Event for this person
     */
    @ManyToOne
    private final Event ev;
    
    /**
     * @brief Constructor of a person
     * @param name Family name of the person
     * @param firstName First name of the person
     * @param email Email of the person
     * @param birthday Birthday of the person
     * @param organisation Organisation for which this person works
     * @param ev Event for which this person participates
     */
    public Person(String name, String firstName, String email, Date birthday,
            String organisation, Event ev){
        this.familyName = name;
        this.firstName = firstName;
        this.email = email;
        this.birthday = birthday.toString();
        this.organisation = organisation;
        this.ev = ev;
    }
    
    /**
     * @brief Export an object modeling Person as a String
     * @return 
     */
    public String toString(){
        return this.firstName + " " + this.familyName;
    }
}
