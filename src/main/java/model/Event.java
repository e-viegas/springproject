/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * ...
 * @author Erwan Viegas
 */
@Entity
@Table(name = "event")
public class Event {
    /**
     * Unique ID of each event
     */
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;
    
    /**
     * Name of the event
     */
    @Column(name = "name")
    private final String name;
    
    /**
     * Theme of the event
     */
    @Column(name = "theme")
    private final String theme;
    
    /**
     * Date when the event begins
     */
    @Column(name = "begindate")
    private final String beginDate;
    
    /**
     * Duration of the event
     */
    @Column(name = "duration")
    private final int duration;
    
    /**
     * People for this event
     */
    @OneToMany(mappedBy = "ev")
    private final List<Person> people;
    
    /**
     * @brief Constructor of an event
     * @param name Name of the event
     * @param theme Theme of the event
     * @param begin Date when the event begins
     * @param duration Duration of this event
     */
    public Event(String name, String theme, Date begin, int duration){
        this.name = name;
        this.theme = theme;
        this.beginDate = begin.toString();
        this.duration = duration;
        this.people = new ArrayList<Person>();
    }
    
    /**
     * @brief Get the number of people for this event
     * @return Number of people
     */
    public int getPeople(){
        return this.people.size();
    }
    
    /**
     * @brief Get one person subscribed for this event
     * @param k Person index
     * @return Person at this index
     */
    public Person get(int k){
        if (0 <= k && k < this.getPeople()) {
            return this.people.get(k);
        } else {
            return null;
        }
    }
}
