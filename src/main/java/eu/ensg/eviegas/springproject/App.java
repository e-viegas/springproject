/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.ensg.eviegas.springproject;

import core.EventManager;
import core.PeopleManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import model.Event;
import model.Person;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 *
 * @author Erwan Viegas
 */
public class App {
    private static SessionFactory create(){
        final StandardServiceRegistry registry = 
                new StandardServiceRegistryBuilder()
                    .configure()
                    .build();
        
        try{
            return new MetadataSources(registry)
                    .buildMetadata()
                    .buildSessionFactory();
        } catch (Exception e){
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
            return null;
        }
    }
    
    public static void main(String argv[]) throws ParseException{
        SessionFactory sf = App.create();
        EventManager evm = EventManager.get(sf);
        PeopleManager pm = PeopleManager.get(sf);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        Event ev = new Event(
                "Prehistoric tribes in Normandy",
                "Archeology",
                sdf.parse("2018-08-20"), 1
        );
        evm.commit(ev);
        
        pm.commit(new Person(
                "Empereur-Buisson",
                "Anaïs",
                "anais.empereur@xyz.fr",
                sdf.parse("1997-01-29"),
                "ENSG", ev
        ));
        
        pm.commit(new Person(
                "Grosmaire",
                "Julie",
                "julie.grosmaire@xyz.fr",
                sdf.parse("1997-01-08"),
                "ENSG", ev
        ));
    }
}
