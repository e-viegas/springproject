/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.Objects;
import model.Event;
import model.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Erwan Viegas
 */
public class EventManager {
    private final SessionFactory sf;
    private static EventManager INSTANCE = null;
    
    private EventManager(SessionFactory sf){
        this.sf = Objects.requireNonNull(sf);
    }
    
    public static EventManager get(SessionFactory sf){
        if (EventManager.INSTANCE == null) {
            EventManager.INSTANCE = new EventManager(sf);
        }
        
        return EventManager.INSTANCE;
    }
    
    /**
     * @brief Add an event to the database
     * @param ev Event to add to the database
     */
    public void commit(Event ev){
        Objects.requireNonNull(ev);
        
        try (Session session = this.sf.openSession()) {
            session.beginTransaction();
            session.save(ev);
            session.getTransaction().commit();
        }
    }
}
