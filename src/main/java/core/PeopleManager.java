/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.Objects;
import model.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Erwan Viegas
 */
public class PeopleManager {
    private final SessionFactory sf;
    private static PeopleManager INSTANCE = null;
    
    private PeopleManager(SessionFactory sf){
        this.sf = Objects.requireNonNull(sf);
    }
    
    public static PeopleManager get(SessionFactory sf){
        if (PeopleManager.INSTANCE == null) {
            PeopleManager.INSTANCE = new PeopleManager(sf);
        }
        
        return PeopleManager.INSTANCE;
    }
    
    /**
     * @brief Add a new person
     * @param p Person to add
     */
    public void commit(Person p){
        Objects.requireNonNull(p);
        
        try (Session session = this.sf.openSession()) {
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();
        }
    }
}
